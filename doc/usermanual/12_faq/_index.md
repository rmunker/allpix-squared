---
# SPDX-FileCopyrightText: 2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "FAQ"
description: "Some of the most frequently asked questions."
weight: 12
---

This chapter provides answers to some of the most frequently asked questions concerning usage, configuration and extension of
the Allpix Squared framework.
